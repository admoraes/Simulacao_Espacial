#inclui a gem Gosu
require 'gosu'

#inclui as classes salvas na pasta model do projeto
require_relative '../model/nave.rb'
require_relative '../model/fundo_tela.rb'
require_relative '../model/meteoro.rb'

class GameWindow < Gosu::Window
  def initialize
    super 800, 600, :fullscreen => true #cria uma janela com 800x600 pixels de largura e ativa o modo tela cheia
    self.caption = "Simulaçao Espacial" #Texto da janela
    
    #criaçao dos objetos a serem utilizados no projeto
    @nave = Nave.new
    @fundo = Fundo_Tela.new
    @meteoro1 = Meteoro.new
    @meteoro2 = Meteoro.new
    @meteoro3 = Meteoro.new
    @nave.posicao_inicial(400, 300) #inicia a posicao da nave em 400 pixels a esquerda da tela e 300 pixels abaixo do topo da tela
  end

  def update #sobrescrita do metodo update de Gosu::Window. Este método é chamado 60 vezes por segundo
     if Gosu::button_down? Gosu::KbLeft #verifica se a tecla direcional esquerda foi pressionada e rotaciona a nave no sentido antihorario
      @nave.rotacionar_esquerda
    end
    if Gosu::button_down? Gosu::KbRight #verifica se a tecla direcional direita foi pressionada e rotaciona a nave no sentido horario
      @nave.rotacionar_direita
    end
    if Gosu::button_down? Gosu::KbUp  #verifica se a tecla direcional pra cima foi pressionada e acelera a nave
      @nave.acelerar
    end
    
    #chama o metodo de movimentação dos objetos
    @nave.movimentar
    @meteoro1.movimentar
    @meteoro2.movimentar
    @meteoro3.movimentar    
  end

  def draw #Sobrescrita do metodo draw da classe Gosu::Window. Este método é chamado toda vez que algo precisa ser desenhado na tela
    #chamada dos metodos de desenho dos objetos
    @fundo.desenhar
    @nave.desenhar
    @meteoro1.desenhar
    @meteoro2.desenhar
    @meteoro3.desenhar        
  end
    
  def button_down(id)
    if id == Gosu::KbEscape #Encerra o programa ao pressionar a tecla esc
      close
    end
  end
end


window = GameWindow.new #cria uma nova janela
window.show #mostra a janela
