class Nave
  def initialize
    @imagem = Gosu::Image.new("imagens/nave.bmp") #associa uma imagem à variavel
    @x = @y = @vel_x = @vel_y = @angulo = @angulo_controle = 0.0  #inicialização das variaveis
  end

  def posicao_inicial(x, y) #metodo que define a posicao inicial da nave no cenario
    @x, @y = x, y
  end

  def rotacionar_esquerda #metodo que rotaciona a nave no sentido antihorario
    @angulo_controle -= 0.05
  end

  def rotacionar_direita  #metodo que rotaciona a nave no sentido horario
    @angulo_controle += 0.05
  end

  def acelerar
    @vel_x += Gosu::offset_x(@angulo, 0.10)
    @vel_y += Gosu::offset_y(@angulo, 0.10)
  end

  def movimentar
    @x += @vel_x
    @y += @vel_y
    @x %= 800
    @y %= 600
    @angulo %= 360
    @angulo += @angulo_controle
  end

  def desenhar
    @imagem.draw_rot(@x, @y, 2, @angulo)
  end
end

