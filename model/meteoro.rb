class Meteoro
  def initialize
    @imagem = Gosu::Image.new("imagens/meteoro.bmp", :tileable => true)
    @x = @y = @vel_x = @vel_y = @angulo = @angulo_rot = 0.0
    #escolhe um angulo aleatorio
    @angulo = rand(360)  
    #inicia as posicoes x e y do meteoro com base no angulo escolhido e um raio de 500pixels 
    @x = 400 + Gosu::offset_x(@angulo, 500)
    @y = 300 + Gosu::offset_y(@angulo, 500)
    #define a velocidade e a direçao em que o meteoro deve se mover        
    @vel_x = Gosu::offset_x(@angulo+180, 0.50)
    @vel_y = Gosu::offset_y(@angulo+180, 0.50)
  end
  
  def movimentar
    @x += @vel_x
    @y += @vel_y
    @x %= 800
    @y %= 600
    @angulo_rot %= 360
    @angulo_rot += 1
  end
  
  def desenhar
    @imagem.draw_rot(@x, @y, 1, @angulo_rot)
  end
end
