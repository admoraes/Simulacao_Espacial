class Fundo_Tela
    def initialize
        #associa à variavel uma imagem
        @imagem_fundo = Gosu::Image.new("imagens/espaco.jpeg", :tileable => true) #Carrega a imagem na variavel imagem_fundo    
    end

    def desenhar
        #desenha a imagem no fundo da tela
        @imagem_fundo.draw_as_quad(0, 0, 0xffffffff, 800, 0, 0xffffffff, 800, 600, 0xffffffff, 0, 600, 0xffffffff, 0)
    end
end
