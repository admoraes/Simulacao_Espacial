require 'gosu'

class Fundo_Tela
    def initialize
        @imagem_fundo = Gosu::Image.new("imagens/space.png", :tileable => true) #Carrega a imagem na variavel imagem_fundo    
    end

    def desenhar
        @imagem_fundo.draw_as_quad(0, 0, 0xffffffff, 800, 0, 0xffffffff, 800, 600, 0xffffffff, 0, 600, 0xffffffff, 0)
    end
end

class Nave
  def initialize
    @imagem = Gosu::Image.new("imagens/nave.bmp")
    @x = @y = @vel_x = @vel_y = @angulo = @angulo_controle = 0.0
  end

  def posicao_inicial(x, y)
    @x, @y = x, y
  end

  def rotacionar_esquerda
    @angulo_controle -= 0.05
  end

  def rotacionar_direita
    @angulo_controle += 0.05
  end

  def acelerar
    @vel_x += Gosu::offset_x(@angulo, 0.10)
    @vel_y += Gosu::offset_y(@angulo, 0.10)
  end

  def movimentar
    @x += @vel_x
    @y += @vel_y
    @x %= 800
    @y %= 600
    @angulo %= 360
    @angulo += @angulo_controle
  end

  def desenhar
    @imagem.draw_rot(@x, @y, 2, @angulo)
  end
end

class GameWindow < Gosu::Window
  def initialize
    super 800, 600, :fullscreen => true
    self.caption = "Caça de Asteroides"
    
    @nave = Nave.new
    @fundo = Fundo_Tela.new
    @nave.posicao_inicial(400, 300)
  end

  def update
     if Gosu::button_down? Gosu::KbLeft or Gosu::button_down? Gosu::GpLeft then
      @nave.rotacionar_esquerda
    end
    if Gosu::button_down? Gosu::KbRight or Gosu::button_down? Gosu::GpRight then
      @nave.rotacionar_direita
    end
    if Gosu::button_down? Gosu::KbUp or Gosu::button_down? Gosu::GpButton0 then
      @nave.acelerar
    end
    @nave.movimentar
  end

  def draw
    @fundo.desenhar
    @nave.desenhar
  end
    
  def button_down(id)
    if id == Gosu::KbEscape
      close
    end
  end
end


window = GameWindow.new
window.show
