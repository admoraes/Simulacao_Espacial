REQUISITOS DO PROJETO:
O único requisito do projeto é, além do ruby, a instalação de uma GEM chamada GOSU, que pode ser encontrada em seu site oficial https://www.libgosu.org/. No mesmo endereço podem ser encontradas as instruções de instalação.

COMO RODAR:
1 - Fazer o download do projeto ou cloná-lo com o GIT.
2 - Executar com o ruby o arquivo main.rb da pasta controller.

UTILIZAÇÃO:
O projeto consiste em uma simulação do ambiente espacial, onde a inércia é muito mais evidente. Logo, ao executar o programa será mostrada uma pequena nave e alguns meteoros.
Os meteoros se movimentam sem a interferência do usuário, apenas a nave pode ser controlada. Para acelerá=la basta pressionar a tecla direcional pra cima (Up Arrow), para rotacioná-la utilizam-se as teclas direcionais esquerda e direita (Left e Right Arrow).
Como o objetivo do projeto é mostrar a física do ambiente espacial, a nave permanece com as velocidades angular e linear constantes, mudando apenas ao se pressionar as teclas de controle novamente e mudar sua trajetória.
